from Moto import Campo, Agua

class FactoryMoto:

    def get_moto(model, ruedas, tipo):
        if model == "C":
            return Campo(ruedas, tipo)
        elif model == "A":
            return Agua(ruedas, tipo)
        else:
            return "Contexto Invalido"