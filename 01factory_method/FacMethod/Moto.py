class Moto:
    def __init__(self, ruedas, tipo):
        self.ruedas = ruedas
        self.tipo = tipo
        self.state = 0
    
    def on(self):
        if self.state == 0:
            print ("encendiendo moto.....")
            self.state = 1
    
    def off(self):
        if self.state == 1:
            print ("apagando la moto.....")
            self.state = 0

class Campo(Moto):

    def __init__(self, ruedas, tipo):
        super(Campo, self).__init__(ruedas, tipo)
        print("Has Adquirido una moto de ", self.tipo, " con ", self.ruedas, " ruedas")


class Agua(Moto):

    def __init__(self, ruedas, tipo):
        super(Agua, self).__init__(ruedas, tipo)
        print("Has Adquirido una moto de ", self.tipo, " con ", self.ruedas, " ruedas")
