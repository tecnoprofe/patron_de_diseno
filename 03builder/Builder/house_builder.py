#import string
from interface_house_builder import IHouseBuilder
from house import House
''' Los Constructores Concretos ofrecen distintas 
implementaciones de los pasos de construcción. 
Los constructores concretos pueden crear productos 
que no siguen la interfaz común. '''


class HouseBuilder(IHouseBuilder):

    def __init__(self):
        self.house = House()

    def set_tipo_construccion(self, tipo_construccion):
        self.house.elementos.append(
            'Tipo de construccion: ' + tipo_construccion)
        return self

    def set_material_pared(self, material_pared):
        self.house.elementos.append('material:---> ' + material_pared)
        return self

    def set_numero_puertas(self, numero_puertas):
        self.house.elementos.append(
            'Numero de puertas:---> ' + str(numero_puertas))
        return self

    def set_numero_ventanas(self, numero_ventanas):
        self.house.elementos.append(
            "Numero de Ventanas:---> " + str(numero_ventanas))
        return self

    def set_numero_dormitorios(self, numero_dormitorios):
        self.house.elementos.append(
            "Numero de Dormitorios:---> " + str(numero_dormitorios))
        return self

    ''' def set_numero_piscinas(self, numero_piscinas):
        self.house.elementos.append(
            "Numero de Piscinas:---> " + str(numero_piscinas))
        return self '''

    def get_result(self):
        return self.house
