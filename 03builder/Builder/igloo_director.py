from house_builder import HouseBuilder
''' La clase Directora define el orden en el 
que se invocarán los pasos de construcción, 
por lo que puedes crear y reutilizar configuraciones 
específicas de los productos. '''


class IglooDirector:

    @staticmethod
    def construct():

        return HouseBuilder()\
            .set_tipo_construccion("Igloo")\
            .set_material_pared("Hielo")\
            .set_numero_puertas(1)\
            .get_result()
