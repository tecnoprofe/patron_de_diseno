from house_builder import HouseBuilder
''' La clase Directora define el orden en el 
que se invocarán los pasos de construcción, 
por lo que puedes crear y reutilizar configuraciones 
específicas de los productos. '''


class CasaPlayaDirector:

    @staticmethod
    def construct():

        return HouseBuilder()\
            .set_tipo_construccion("Casa de playa")\
            .set_material_pared("Madera")\
            .set_numero_ventanas(8)\
            .get_result()
