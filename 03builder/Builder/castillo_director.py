from house_builder import HouseBuilder
''' La clase Directora define el orden en el 
que se invocarán los pasos de construcción, 
por lo que puedes crear y reutilizar configuraciones 
específicas de los productos. '''


class CastilloDirector:

    @staticmethod
    def construct():

        return HouseBuilder()\
            .set_tipo_construccion("Castillo")\
            .set_material_pared("Arenilla")\
            .set_numero_puertas(100)\
            .set_numero_ventanas(50)\
            .get_result()
