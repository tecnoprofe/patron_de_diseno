from cProfile import label
from email.mime import image
from turtle import st
from matplotlib.pyplot import get
from igloo_director import IglooDirector
from castillo_director import CastilloDirector
from Casa_playa import CasaPlayaDirector
from MiCasaDirector import *
from tkinter import *
from tkinter import messagebox
import tkinter as tk
from PIL import ImageTk, Image
from os import path

''' El Cliente debe asociar uno de los objetos 
constructores con la clase directora. Normalmente, 
se hace una sola vez mediante los parámetros del 
constructor de la clase directora'''

ventana = Tk()
ventana.geometry("750x700")
ventana.title(" EJEMPLO BUILDER")

IGLOO = IglooDirector.construct()
CASTILLO = CastilloDirector.construct()
CASAPLAYA = CasaPlayaDirector.construct()
MICASA = MiCasaDirector.construct(
    tipo="casa", material="ladrillos", puertas=5, ventanas=6, dormitorios=3)


def Igloo():
    messagebox.showinfo("IGLOO BUILDER", message=IGLOO.construction())


def Castillo():
    messagebox.showinfo("CASTILLO BUILDER", message=CASTILLO.construction())


def CasaPlaya():
    messagebox.showinfo("CASA PLAYA BUILDER", message=CASAPLAYA.construction())


def MyCasa():
    messagebox.showinfo("Mi Propia Casa BUILDER",
                        message=MICASA.construction())


# IMAGENES
abrir = path.dirname(__file__)
print("================="+abrir)
imagen_castillo = ImageTk.PhotoImage(Image.open(abrir+"/img/castillo.gif"))
castillo = tk.Label(image=imagen_castillo)
btnCasti = Button(ventana, image=imagen_castillo,
                  command=Castillo, height=200, width=200).place(x=100, y=100)

imagen_casaplaya = ImageTk.PhotoImage(Image.open(abrir+"/img/casaplaya.jpg"))
casaplaya = tk.Label(image=imagen_casaplaya)
btnCasaplaya = Button(ventana, image=imagen_casaplaya,
                      command=CasaPlaya, height=200, width=200).place(x=400, y=100)

imagen_igloo = ImageTk.PhotoImage(Image.open(abrir+"/img/igloo.jpg"))
igloo = tk.Label(image=imagen_igloo)
btnIgloo = Button(ventana, image=imagen_igloo, command=Igloo,
                  height=200, width=200).place(x=250, y=350)


btnmyhouse = Button(text="Mi Casa", command=MyCasa)
btnmyhouse.place(x=580, y=500)


ventana.mainloop()
