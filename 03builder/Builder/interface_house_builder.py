from abc import ABCMeta, abstractmethod
''' La interfaz Constructora declara pasos de construcción 
de producto que todos los tipos de objetos constructores 
tienen en común. '''
''' ABCMeta refers to Abstract Base Classes.
The benefits of using ABCMeta classes to create abstract classes 
is that your IDE and Pylint will indicate to you at development 
time whether your inheriting classes conform to the class 
definition that you've asked them to. '''


class IHouseBuilder(metaclass=ABCMeta):
    "Interface IHouse builder"

    @staticmethod
    @abstractmethod
    def set_tipo_construccion(tipo_construccion):
        "Tipo de contruccion"

    @staticmethod
    @abstractmethod
    def set_material_pared(material_pared):
        "Material de construccion"

    @staticmethod
    @abstractmethod
    def set_numero_puertas(numero_puertas):
        "Numero de puertas"

    @staticmethod
    @abstractmethod
    def set_numero_ventanas(numero_ventanas):
        "Numero de ventanas"

    @staticmethod
    @abstractmethod
    def set_numero_dormitorios(numero_dormitorios):
        "Numero de dormitorios"

    ''' @staticmethod
    @abstractmethod
    def set_numero_piscinas(numero_piscinas):
        "Numero de piscinas" '''

    @staticmethod
    @abstractmethod
    def get_result():
        "Retornar el producto final"
