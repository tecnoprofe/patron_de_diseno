from house_builder import HouseBuilder
''' La clase Directora define el orden en el 
que se invocarán los pasos de construcción, 
por lo que puedes crear y reutilizar configuraciones 
específicas de los productos. '''


class MiCasaDirector:

    @staticmethod
    def construct(tipo, material, puertas, ventanas, dormitorios):

        return HouseBuilder()\
            .set_tipo_construccion(tipo)\
            .set_material_pared(material)\
            .get_result()
