from Controles import Controles
from tkinter import Button

class Boton(Controles, Button):
    x=None
    y=None
    accion=None

class MacBoton(Boton):
    def __init__(self, ventana):
        super().__init__()
        self.configure(text="Enviar", border=5, background="gray")
        self.tk=ventana
        self.pack()

class WinBoton(Boton):
    def __init__(self, ventana):
        super().__init__()
        self.configure(text="Enviar")
        self.tk=ventana
        self.pack()

class LinBoton(Boton):
    def __init__(self, ventana):
        super().__init__()
        self.configure(text="Linux")
        self.tk=ventana
        self.pack()