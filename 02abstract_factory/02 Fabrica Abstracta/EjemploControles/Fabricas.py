from abc import ABC, abstractmethod
from Boton import *
from Entrada import *

class FabricaControles(ABC):
    @abstractmethod
    def getButton(self, ventana):
        pass
    @abstractmethod
    def getEntry(self, ventana):
        pass


class FabricaControlesMac(FabricaControles):
    def getButton(self, ventana):
        return MacBoton(ventana)
    def getEntry(self, ventana):
        return MacEntrada(ventana)

class FabricaControlesWin(FabricaControles):
    def getButton(self, ventana):
        return WinBoton(ventana)
    def getEntry(self, ventana):
        return WinEntrada(ventana)

class FabricaControlesLin(FabricaControles):
    def getButton(self, ventana):
        return LinBoton(ventana)
    def getEntry(self, ventana):
        return LinEntrada(ventana)
