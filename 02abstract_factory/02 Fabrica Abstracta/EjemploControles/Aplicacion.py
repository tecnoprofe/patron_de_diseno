from Fabricas import *
from tkinter import Tk


if __name__ == "__main__":
    ventana=Tk()
    ventana.title("Linux")

    if (ventana.title()=="MacOS"):
        fab=FabricaControlesMac()
    elif (ventana.title()=="Windows"):
        fab=FabricaControlesWin()
    elif (ventana.title()=="Linux"):
        fab=FabricaControlesLin()

    ventana.geometry("400x100")
    en=fab.getEntry(ventana)
    b=fab.getButton(ventana)
    ventana.mainloop()