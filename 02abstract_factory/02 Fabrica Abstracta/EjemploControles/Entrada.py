from Controles import *
from tkinter import Entry

class Entrada(Controles, Entry):
    x=None
    y=None
    texto=None

class MacEntrada(Entrada):
    def __init__(self, ventana):
        super().__init__()
        self.configure(font=("Comic Sans MS",14), border=5, background="gray", foreground="white" )
        self.tk=ventana
        self.pack()

class WinEntrada(Entrada):
    def __init__(self, ventana):
        super().__init__()
        self.configure(font=("Arial",14))
        self.tk=ventana
        self.pack()

class LinEntrada(Entrada):
    def __init__(self, ventana):
        super().__init__()
        self.configure(font=("Comic Sans MS",14), border=5, background="blue", foreground="white" )
        self.tk=ventana
        self.pack()