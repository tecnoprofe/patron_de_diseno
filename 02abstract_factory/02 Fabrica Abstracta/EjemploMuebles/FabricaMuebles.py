from abc import ABC, abstractmethod
import tkinter as tk
from PIL import ImageTk,Image 
from os import path


root = tk.Tk()
root.geometry('800x800')

#imagenes
padre=path.dirname(__file__)
print("================="+padre)
sv_img = ImageTk.PhotoImage(Image.open(padre+"/img/silla_victoriana.jpg"))
mv_img = ImageTk.PhotoImage(Image.open(padre+"/img/mesa_victoriana.png"))
sov_img = ImageTk.PhotoImage(Image.open(padre+"/img/sofa_victoriano.jpg"))

sm_img = ImageTk.PhotoImage(Image.open(padre+"/img/silla_moderna.jpg"))
mm_img = ImageTk.PhotoImage(Image.open(padre+"/img/mesa_moderna.jpg"))
som_img = ImageTk.PhotoImage(Image.open(padre+"/img/sofa_moderno.jpg"))

sd_img = ImageTk.PhotoImage(Image.open(padre+"/img/silla_decorativa.jpg"))
md_img = ImageTk.PhotoImage(Image.open(padre+"/img/mesa_decorativa.jpg"))
sod_img = ImageTk.PhotoImage(Image.open(padre+"/img/sofa_decorativo.jpg"))

#variables
silla_victoriana = tk.Label(image=sv_img)
mesa_victoriana = tk.Label(image=mv_img)
sofa_victoriano = tk.Label(image=sov_img)

silla_moderna = tk.Label(image=sm_img)
mesa_moderna = tk.Label(image=mm_img)
sofa_moderno = tk.Label(image=som_img)

silla_decorativa = tk.Label(image=sd_img)
mesa_decorativa = tk.Label(image=md_img)
sofa_decorativo = tk.Label(image=sd_img)

# INICIALIZAMOS SILLA
class Silla(ABC):
    @abstractmethod
    def comprar(self):
        pass

class SillaVictoriana(Silla):
    def comprar(self):
        return silla_victoriana.pack() 

class SillaDecorativa(Silla):
    def comprar(self):
        return silla_decorativa.pack() 

class SillaModerna(Silla):
    def comprar(self):
        return silla_moderna.pack() 


# INICIALIZAMOS MESA
class Mesa(ABC):
    @abstractmethod
    def comprar(self):
        pass

class MesaVictoriana(Mesa):
    def comprar(self):
        return mesa_victoriana.pack() 

class MesaDecorativa(Mesa):
    def comprar(self):
        return mesa_decorativa.pack() 

class MesaModerna(Mesa):
    def comprar(self):
        return mesa_moderna.pack() 


# INICIALIZAMOS SOFA
class Sofa(ABC):
    @abstractmethod
    def comprar(self):
        pass

class SofaVictoriana(Sofa):
    def comprar(self):
        return sofa_victoriano.pack() 

class SofaDecorativa(Sofa):
    def comprar(self):
        return sofa_decorativo.pack() 

class SofaModerna(Sofa):
    def comprar(self):
        return sofa_moderno.pack() 


class FabricaMuebles(ABC):
    @abstractmethod
    def crear_silla(self):
        pass

    @abstractmethod
    def crear_mesa(self):
        pass

    @abstractmethod
    def crear_sofa(self):
        pass


class Victoriana(FabricaMuebles):
    def crear_silla(self):
        return SillaVictoriana()

    def crear_mesa(self):
        return MesaVictoriana()

    def crear_sofa(self):
        return SofaVictoriana()


class Decorativa(FabricaMuebles):
    def crear_silla(self):
        return SillaDecorativa()

    def crear_mesa(self):
        return MesaDecorativa()

    def crear_sofa(self):
        return SofaDecorativa()


class Moderna(FabricaMuebles):
    def crear_silla(self):
        return SillaModerna()

    def crear_mesa(self):
        return MesaModerna()

    def crear_sofa(self):
        return SofaModerna()


if __name__ == "__main__":
        fabrica = Moderna()
        silla = fabrica.crear_mesa()
        print(silla.comprar())
        root.mainloop()